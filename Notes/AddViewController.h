//
//  AddViewController.h
//  Notes
//
//  Created by Soleorax on 16.08.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AddViewController, Note;

@protocol AddViewControllerDelegate <NSObject>

- (void)addViewController:(AddViewController *)aViewController
            didCreateNote:(Note *)aNote;

- (void)addViewController:(AddViewController *)aViewController
            didUpdateNote:(Note *)aNote;

@end

@interface AddViewController : UIViewController

@property (nonatomic, weak) id<AddViewControllerDelegate> delegate;
@property (strong, nonatomic) Note *note;
@property (assign, nonatomic) NSInteger *uniqueId;


@end
