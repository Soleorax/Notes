//
//  NoteTableViewCell.m
//  Notes
//
//  Created by Soleorax on 30.08.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import "NoteTableViewCell.h"


@interface NoteTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *noteImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *textViewLabel;

@end

@implementation NoteTableViewCell

- (void)configureWithNote:(Note *)note {
    if (note.imagePath != nil) {
        UIImage *image = [UIImage imageWithContentsOfFile:note.fullImagePath];
        self.noteImageView.image = image;
        self.noteImageView.contentMode = UIViewContentModeScaleAspectFill;
    } else {
        self.noteImageView.image = [UIImage imageNamed:@"note_placeholder"];
        self.noteImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    
    self.titleLabel.text = note.name;
    
    static NSDateFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [NSDateFormatter new];
        formatter.dateStyle = NSDateFormatterNoStyle;
        formatter.timeStyle = NSDateFormatterMediumStyle;
    });
   
    self.timeLabel.text = [formatter stringFromDate:note.date];
    self.textViewLabel.text = note.text;
}

@end
