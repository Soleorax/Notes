//
//  WebImageViewController.h
//  Notes
//
//  Created by Soleorax on 01.09.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol WebImageControllerDelegate;

@interface WebImageViewController : UIViewController

@property (weak, nonatomic) id <WebImageControllerDelegate> delegate;

@end

@protocol WebImageControllerDelegate <NSObject>

- (void)webImageController:(WebImageViewController *)controller
              didPickImage:(UIImage *) image;

@end
