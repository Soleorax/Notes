//
//  NoteDatabase.h
//  Notes
//
//  Created by Soleorax on 18.08.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Note.h"

@interface NoteDatabase : NSObject

+ (NoteDatabase *)database;

- (NSArray<Note *> *)notes;

- (BOOL)insertNote:(Note *)note;

- (BOOL)deleteNote:(Note *)note;

- (BOOL)updateNote:(Note *)note;

- (NSArray<Note *> *)searchInNote:(NSString *)symbol;

@end
