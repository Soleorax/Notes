//
//  WebImageViewController.m
//  Notes
//
//  Created by Soleorax on 01.09.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import "WebImageViewController.h"
#import "ImageCollectionViewCell.h"
#import "TumblrApiService.h"

@interface WebImageViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray<NSString *> *urlArray;

@end

@implementation WebImageViewController

static NSString * const kCollectionViewCellIdentifier = @"kCollectionViewCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.textField becomeFirstResponder];
}

#pragma mark - UICollectionDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.urlArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewCellIdentifier
                                                                             forIndexPath:indexPath];
    [cell configureWithUrl:self.urlArray[indexPath.row]];
    return cell;
}

#pragma mark - UICollectionsViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageCollectionViewCell *cell = (ImageCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [self.delegate webImageController:self didPickImage:cell.image];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
   
        [[TumblrApiService instance]fetchPhotosWithTag:textField.text completion:^(NSArray<NSString *> *photos, NSError *error) {
            self.urlArray = [NSMutableArray arrayWithArray:photos];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.collectionView reloadData];
            });
        }];
        return YES;
}

@end

