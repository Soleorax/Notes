//
//  ViewController.m
//  Notes
//
//  Created by Soleorax on 16.08.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import "RootViewController.h"
#import "AddViewController.h"
#import "NoteTableViewCell.h"
#import "Note.h"
#import "NoteDatabase.h"
#import "NotesDayList.h"
#import "NSDate+Extension.h"
#import "TumblrApiService.h"

@interface RootViewController () <UITableViewDataSource, UITableViewDelegate, AddViewControllerDelegate, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray<NotesDayList *> *mNoteListsArray;
@property (strong, nonatomic) NSDateFormatter *sectionTitleDateFormatter;
@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) NSArray<Note *> *searchArray;

@end

@implementation RootViewController

static NSString * const kCellIdentifier = @"kCellIdentifier";

#pragma mark - UIViewControlle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    
    self.searchBar.showsCancelButton = YES;
    self.searchBar.delegate = self;
    
    self.tableView.tableHeaderView = self.searchBar;
    
    NSArray<Note *> *notes = [[NoteDatabase database] notes];
    [self initDataSourceWithNotesArray:notes];
    
    self.sectionTitleDateFormatter = [NSDateFormatter new];
    self.sectionTitleDateFormatter.dateStyle = NSDateFormatterLongStyle;
    self.sectionTitleDateFormatter.timeStyle = NSDateFormatterNoStyle;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView setContentOffset:CGPointMake(0, 44)];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"AddViewControllerSegueId"]) {
        AddViewController *addViewController = segue.destinationViewController;
        addViewController.delegate = self;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AddViewController *addViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddViewController"];
    addViewController.delegate = self;
    addViewController.note = self.mNoteListsArray[indexPath.section].notes[indexPath.row];
    [self.navigationController pushViewController:addViewController animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark - UITableViewDataSource

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
       NSString *path = self.mNoteListsArray[indexPath.section].notes[indexPath.row].fullImagePath;
        
       [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
       
       [[NoteDatabase database] deleteNote:self.mNoteListsArray[indexPath.section].notes[indexPath.row]];
       
       if (self.mNoteListsArray[indexPath.section].notes.count == 1) {
            [self.mNoteListsArray removeObjectAtIndex:indexPath.section];
           [tableView deleteSections: [NSIndexSet indexSetWithIndex:indexPath.section]
                    withRowAnimation:UITableViewRowAnimationAutomatic];
   
        } else {
            [self.mNoteListsArray[indexPath.section].notes removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NoteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    [cell configureWithNote:self.mNoteListsArray[indexPath.section].notes[indexPath.row]];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.mNoteListsArray[section].notes.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.mNoteListsArray.count;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [self.sectionTitleDateFormatter stringFromDate:self.mNoteListsArray[section].dayDate];
}
#pragma mark - AddViewControllerDelegate

- (void)addViewController:(AddViewController *)aViewController didCreateNote:(Note *)aNote {
    
    NotesDayList *list = [self listForNote:aNote];
    
    if (nil != list) {
        [list.notes addObject:aNote];
        NSInteger section = [self.mNoteListsArray indexOfObject:list];
        NSInteger row = [list.notes indexOfObject:aNote];
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row
                                                                    inSection:section]]
                              withRowAnimation:UITableViewRowAnimationAutomatic];
    } else {
        list = [NotesDayList new];
        list.dayDate = aNote.date;
        [list.notes addObject:aNote];
        [self.mNoteListsArray addObject:list];
        
        NSInteger section = [self.mNoteListsArray indexOfObject:list];
        [self.tableView insertSections: [NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (void)addViewController:(AddViewController *)aViewController didUpdateNote:(Note *)aNote {
    
    NotesDayList *list = [self listForNote:aNote];
    
    NSInteger section = [self.mNoteListsArray indexOfObject:list];
    NSInteger row = [list.notes indexOfObject:aNote];

    
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row
                                                                inSection:section]]
                          withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Private

- (NotesDayList *)listForNote:(Note *)aNote {
    for (NotesDayList *list in self.mNoteListsArray) {
        if ([[list.dayDate dateWithTruncatedTime] isEqual: [aNote.date dateWithTruncatedTime]]) {
            return list;
        }
    }
    return nil;
}

- (void)initDataSourceWithNotesArray:(NSArray<Note *> *)aNotesArray {
    self.mNoteListsArray = [NSMutableArray array];
    for (Note* note in aNotesArray) {
        BOOL shouldCreateNewList = YES;
        for (NotesDayList *list in self.mNoteListsArray) {
            if ([[list.dayDate dateWithTruncatedTime] isEqual:[note.date dateWithTruncatedTime]]) {
                [list.notes addObject:note];
                shouldCreateNewList = NO;
                break;
            }
        }
        if (shouldCreateNewList) {
            NotesDayList *list = [NotesDayList new];
            list.dayDate = note.date;
            [list.notes addObject:note];
            [self.mNoteListsArray addObject:list];
        }
    }
}

#pragma mark - UISearchBarDelegate

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        return YES;
    }
    NSString *searchString = [searchBar.text stringByReplacingCharactersInRange:range withString:text];
    [self initDataSourceWithNotesArray:[[NoteDatabase database] searchInNote:searchString]];

    [self.tableView reloadData];
    
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.searchBar.text = @"";
    [self.tableView setContentOffset:CGPointMake(0, 44) animated:YES];
    [self initDataSourceWithNotesArray:[[NoteDatabase database] notes]];
    
    [self.tableView reloadData];
    [self.searchBar resignFirstResponder];

}

@end
