//
//  Note.m
//  Notes
//
//  Created by Soleorax on 16.08.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import "Note.h"

@implementation Note

- (BOOL)isEqual:(id)object {
    return [object isKindOfClass: [Note class]] && ((Note *)object).uniquedId == self.uniquedId;
}

- (NSUInteger)hash {
    return self.uniquedId + self.name.hash + self.text.hash;
}

- (NSString *)fullImagePath {
    return [[[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                              inDomains:NSUserDomainMask] lastObject] path]
            stringByAppendingPathComponent:self.imagePath];
}

@end
