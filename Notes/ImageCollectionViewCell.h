//
//  ImageCollectionViewCell.h
//  Notes
//
//  Created by Soleorax on 01.09.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong, readonly) UIImage *image;

- (void)configureWithUrl:(NSString *)url;

@end
