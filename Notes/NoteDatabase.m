//
//  NoteDatabase.m
//  Notes
//
//  Created by Soleorax on 18.08.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import "NoteDatabase.h"
#import "RootViewController.h"

@interface NoteDatabase () {
    sqlite3 *_database;
}

@end


@implementation NoteDatabase

static NoteDatabase *_database;

+ (NSString *)DBPath {
    return [[[[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] path] stringByAppendingPathComponent:@"NoteBase"] stringByAppendingPathExtension:@"sqlite3"];
}

+ (NoteDatabase *)database {
    if (_database == nil) {
        NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"NoteBase" ofType:@"sqlite3"];
        
        NSString *pathToDB = [self DBPath];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:pathToDB]) {
            [[NSFileManager defaultManager] copyItemAtPath:bundlePath
                                                    toPath:pathToDB
                                                     error:nil];
        }
        _database = [NoteDatabase new];
    }

    return _database;
}

- (instancetype)init {
    self = [super init];
    
    if (self) {
        NSString *sqLiteDb = [[self class] DBPath];
        
        if (sqlite3_open([sqLiteDb UTF8String], &_database) != SQLITE_OK) {
            NSLog(@"Failed to open database!");
        }
    }
    return self;
}

- (void)dealloc {
    sqlite3_close(_database);
}

- (NSArray<Note *> *)notes {
    
    NSMutableArray *mNotesArray = [NSMutableArray array];
    NSString *query = @"SELECT * FROM Note";
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int uniquedId = sqlite3_column_int(statement, 0);
            char *titleChars = (char *) sqlite3_column_text(statement, 1);
            char *textChars = (char *) sqlite3_column_text(statement, 2);
            char *imagePathChars = (char *) sqlite3_column_text(statement, 3);
            int  dateInt = sqlite3_column_int(statement, 4);
            
            Note *newNote = [Note new];
            newNote.name = titleChars != NULL ? [[NSString alloc] initWithUTF8String:titleChars] : @"";
            newNote.text = textChars != NULL ? [[NSString alloc] initWithUTF8String:textChars] : @"";
            newNote.imagePath = imagePathChars != NULL ? [[NSString alloc] initWithUTF8String:imagePathChars] : nil;
            newNote.date = [NSDate dateWithTimeIntervalSince1970:dateInt];
            newNote.uniquedId = uniquedId;
            
            [mNotesArray addObject:newNote];
        }
    }
    
    return mNotesArray;
}

- (BOOL)insertNote:(Note *)note {
    
    NSString *query = @"INSERT INTO Note(title, text, date, image_path) values(?, ?, ?, ?)";
    sqlite3_stmt *statement = nil;
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        sqlite3_bind_text(statement, 1, [note.name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 2, [note.text UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(statement, 3, [note.date timeIntervalSince1970]);
        sqlite3_bind_text(statement, 4, [note.imagePath UTF8String], -1, SQLITE_OK);
        
        sqlite3_step(statement);
        sqlite3_finalize(statement);
        
        note.uniquedId = (int)sqlite3_last_insert_rowid(_database);
        
        return YES;
    } else {
        NSLog(@"prepare failed");
    }
    return NO;
}

-(BOOL)deleteNote:(Note *)note {
    
    NSString *query = @"DELETE FROM Note WHERE id = ?";
  
    sqlite3_stmt *statement = nil;
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        sqlite3_bind_int(statement, 1, (int)note.uniquedId);
        
        sqlite3_step(statement);
        sqlite3_finalize(statement);
        
        if (sqlite3_errcode(_database) == SQLITE_OK) {
            return YES;
        }
        return NO;
    }
    return NO;
}

-(BOOL)updateNote:(Note *)note {
    
    NSString *query = @"UPDATE Note SET title = ?, text = ? WHERE id = ?";
    sqlite3_stmt *statement = nil;
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        
        sqlite3_bind_text(statement, 1, [note.name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 2, [note.text UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(statement, 3, (int)note.uniquedId);
        
        sqlite3_step(statement);
        sqlite3_finalize(statement);
        
        return YES;
    }
    NSLog(@"prepare failed");
    
    return NO;
}

- (NSArray<Note*> *)searchInNote:(NSString *)symbol {
    
    NSMutableArray *mNoteArray = [NSMutableArray array];
    
    NSString *query = @"SELECT * FROM Note WHERE title LIKE ? OR text LIKE ?";
    
    NSString *paramaterString = [NSString stringWithFormat:@"%%%@%%", symbol];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        
        sqlite3_bind_text(statement, 1, [paramaterString UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 2, [paramaterString UTF8String], -1, SQLITE_TRANSIENT);
        
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int uniquedId = sqlite3_column_int(statement, 0);
            char *titleChars = (char *) sqlite3_column_text(statement, 1);
            char *textChars = (char *) sqlite3_column_text(statement, 2);
            char *imagePathChars = (char *) sqlite3_column_text(statement, 3);
            int  dateInt = sqlite3_column_int(statement, 4);
            
            Note *newNote = [Note new];
            newNote.name = titleChars != NULL ? [[NSString alloc] initWithUTF8String:titleChars] : @"";
            newNote.text = textChars != NULL ? [[NSString alloc] initWithUTF8String:textChars] : @"";
            newNote.imagePath = imagePathChars != NULL ? [[NSString alloc] initWithUTF8String:imagePathChars] : nil;
            newNote.date = [NSDate dateWithTimeIntervalSince1970:dateInt];
            newNote.uniquedId = uniquedId;
            
            [mNoteArray addObject:newNote];
        }
    }
    return mNoteArray;
}

@end
