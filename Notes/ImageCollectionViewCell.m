//
//  ImageCollectionViewCell.m
//  Notes
//
//  Created by Soleorax on 01.09.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import "ImageCollectionViewCell.h"
#import "ImageDownloader.h"

@interface ImageCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation ImageCollectionViewCell

- (void)configureWithUrl:(NSString *)url {
    
    [[ImageDownloader instance] downloadImageWithURL:url
                                          completion:^(UIImage *anImage)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             self.imageView.image = anImage;
         });
     }];
}

- (UIImage *)image {
    return self.imageView.image;
}

@end
