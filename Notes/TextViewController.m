//
//  TextViewController.m
//  Notes
//
//  Created by Soleorax on 16.08.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import "TextViewController.h"
#import "NoteDatabase.h"

@interface TextViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *labelView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewBottomConstraintHeigth;

@end

@implementation TextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = self.note.name;
    self.textView.text = self.note.text;
    
    NSString *imagePath = self.note.imagePath;
    
    UIImage *image = [UIImage imageWithContentsOfFile:[[[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                                                                inDomains:NSUserDomainMask] lastObject] path]
                                                       stringByAppendingPathComponent:imagePath]];
    
    self.imageView.image = image;
    self.imageView.contentMode = UIControlStateNormal;
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateStyle = NSDateFormatterLongStyle;
    formatter.timeStyle = NSDateFormatterMediumStyle;
    NSString *date = [formatter stringFromDate:self.note.date];
    
    self.labelView.text = date;
    
}


@end
