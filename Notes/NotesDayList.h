//
//  NotesDayList.h
//  Notes
//
//  Created by Soleorax on 30.08.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Note.h"

@interface NotesDayList : NSObject

@property (strong, nonatomic) NSDate *dayDate;
@property (strong, nonatomic) NSMutableArray <Note *> *notes;


@end
