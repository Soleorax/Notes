//
//  NSDate+Extension.h
//  Notes
//
//  Created by Soleorax on 31.08.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extension)

- (NSDate *)dateWithTruncatedTime;

@end
