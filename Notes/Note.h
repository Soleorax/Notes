//
//  Note.h
//  Notes
//
//  Created by Soleorax on 16.08.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Note : NSObject

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, assign) NSInteger uniquedId;
@property (nonatomic, strong) NSString *imagePath;
@property (nonatomic, strong, readonly) NSString *fullImagePath;

@end
