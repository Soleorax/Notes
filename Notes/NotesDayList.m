//
//  NotesDayList.m
//  Notes
//
//  Created by Soleorax on 30.08.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import "NotesDayList.h"

@implementation NotesDayList

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.notes = [NSMutableArray array];
    }
    return self;
}

@end
