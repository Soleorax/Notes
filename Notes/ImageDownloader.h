//
//  ImageDownloader.h
//  Notes
//
//  Created by Soleorax on 01.09.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

@import UIKit;

@interface ImageDownloader : NSObject

+ (instancetype)instance;

- (void)downloadImageWithURL:(NSString *)anURL completion:(void(^)(UIImage *anImage))aCompletion;

@end
