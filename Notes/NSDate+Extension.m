//
//  NSDate+Extension.m
//  Notes
//
//  Created by Soleorax on 31.08.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import "NSDate+Extension.h"

@implementation NSDate (Extension)

- (NSDate *)dateWithTruncatedTime {
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                                              fromDate:self];
    return [[NSCalendar currentCalendar] dateFromComponents:components];
}

@end
