//
//  NoteTableViewCell.h
//  Notes
//
//  Created by Soleorax on 30.08.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Note.h"

@interface NoteTableViewCell : UITableViewCell

- (void)configureWithNote:(Note *)note;

@end
