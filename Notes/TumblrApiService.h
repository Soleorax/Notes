//
//  TumblrApiService.h
//  Notes
//
//  Created by Soleorax on 01.09.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TumblrApiService : NSObject

+ (instancetype)instance;

- (void)fetchPhotosWithTag:(NSString *)aTag
                completion:(void(^)(NSArray<NSString *> *photos, NSError *error))aCompletion;

@end
