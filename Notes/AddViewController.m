//
//  AddViewController.m
//  Notes
//
//  Created by Soleorax on 16.08.16.
//  Copyright © 2016 Soleorax. All rights reserved.

#import "AddViewController.h"
#import "Note.h"
#import "NoteDatabase.h"
#import "WebImageViewController.h"

@interface AddViewController () <UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate,
UITextViewDelegate, WebImageControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFieldHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeightConstraint;

@property (nonatomic, assign) BOOL isFirstLayout;
@property (assign, nonatomic) BOOL isEditing;

@end

@implementation AddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.note.name;
    self.isFirstLayout = YES;
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateStyle = NSDateFormatterLongStyle;
    formatter.timeStyle = NSDateFormatterMediumStyle;
    
    NSString *date = [formatter stringFromDate:[NSDate date]];
    
    if (self.note.imagePath != nil) {
        UIImage *image = [UIImage imageWithContentsOfFile:self.note.fullImagePath];
        [self updateImageWithImage:image];
    }
    
    if (self.note == nil) {
        [self.textField becomeFirstResponder];
        
        self.note = [Note new];
        self.note.date = [NSDate date];
        self.dateLabel.text = date;
    } else {
        self.textView.editable = NO;
        self.isEditing = YES;
        self.navigationItem.rightBarButtonItems =
        @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                        target:self
                                                        action:@selector(editPressed:)]];
       
        self.textFieldHeightConstraint.constant = 0;
        self.textView.text = self.note.text;
        self.dateLabel.text = [formatter stringFromDate:self.note.date];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChangeFrame:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if (self.isFirstLayout) {
        self.textViewHeightConstraint.constant = self.isEditing ? [self textViewHeightForText:self.textView.text] : 200;
        self.isFirstLayout = NO;
    }
}

- (void)editPressed:(UIBarButtonItem *)aSender {
    [self.textField becomeFirstResponder];
    self.textView.editable = YES;
    self.textField.text = self.note.name;
    
    
    self.navigationItem.rightBarButtonItems =
    @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                    target:self
                                                    action:@selector(donePressed:)]];
    [UIView animateWithDuration:.25
                     animations:^
     {
         self.textFieldHeightConstraint.constant = 30.;
         [self.view layoutIfNeeded];
     }];
}

- (IBAction)donePressed:(UIBarButtonItem *)sender {
    self.note.text = self.textView.text;
    self.note.name = self.textField.text;
    
    if (self.note.text.length > 0 && self.textField.text.length > 0) {
        if (self.isEditing) {
            [[NoteDatabase database] updateNote:self.note];
            [self.delegate addViewController:self didUpdateNote:self.note];
        } else {
            [[NoteDatabase database] insertNote:self.note];
            [self.delegate addViewController:self didCreateNote:self.note];
        }
        [self.navigationController popViewControllerAnimated:YES];
        
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Опаньки :("
                                                                                 message:@"Введите заглавие или текст!"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction: [UIAlertAction actionWithTitle:@"OK"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {}]];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (IBAction)addPressed:(UIBarButtonItem *)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Select!"
                                                                             message:@"Method download"
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:
     [UIAlertAction actionWithTitle:@"Camera"
                              style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction *action)
      {
          if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
          {
              UIImagePickerController *imagePickerController = [UIImagePickerController new];
              imagePickerController.delegate = self;
              imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
              
              [self presentViewController:imagePickerController
                                 animated:YES
                               completion:nil];
          } else {
              UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Camera Unavailable"
                                                                             message:@"Unable to find a camera on your device."
                                                                      preferredStyle:UIAlertControllerStyleAlert];
              [self presentViewController:alert
                                 animated:YES
                               completion:nil];
          }
      }]];
    
    [alertController addAction: [UIAlertAction actionWithTitle:@"Photo"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action)
                                 {
                                     UIImagePickerController *imagePickerController = [UIImagePickerController new];
                                     imagePickerController.delegate = self;
                                     [self presentViewController:imagePickerController animated:YES completion:nil];
                                 }]];
    [alertController addAction: [UIAlertAction actionWithTitle:@"Web images"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action)
                                 {
                                     WebImageViewController *webImageController =
                                     [self.storyboard instantiateViewControllerWithIdentifier:@"WebImageViewController"];
                                     webImageController.delegate = self;
                                     [self.navigationController pushViewController:webImageController animated:YES];
                                 }]];
    
    [alertController addAction: [UIAlertAction actionWithTitle:@"Cancel"
                                                         style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction *action) {}]];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        alertController.popoverPresentationController.barButtonItem = sender;
    }

    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {

    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    NSData *binaryImageData = UIImagePNGRepresentation(image);
    
    NSDate *date = [NSDate date];
    
    NSString *fileName = [[NSString stringWithFormat:@"%f", [date timeIntervalSince1970]] stringByAppendingPathExtension:@"png"];
    NSString *path = [[[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                               inDomains:NSUserDomainMask] lastObject] path]
                      stringByAppendingPathComponent:fileName];
    
    self.note.imagePath = fileName;
    self.imageView.image = image;
    self.imageViewHeightConstraint.constant = (335 * image.size.height) / image.size.width;

    [binaryImageData writeToFile:path atomically:YES];

    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Private

- (CGFloat)textViewHeightForText:(NSString *)aText {
    return [aText boundingRectWithSize:
            CGSizeMake(CGRectGetWidth(self.textView.frame) - self.textView.textContainerInset.left - self.textView.textContainerInset.right, CGFLOAT_MAX)
                                      options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                   attributes:@{NSFontAttributeName: self.textView.font}
                                      context:nil].size.height
    + self.textView.textContainerInset.top + self.textView.textContainerInset.bottom;
}

- (void)updateImageWithImage:(UIImage *)anImage {
    static CGFloat const kImageViewPadding = 20.;
    self.imageViewHeightConstraint.constant = ((self.view.bounds.size.width - kImageViewPadding * 2)
                                               * anImage.size.height) / anImage.size.width;
    self.imageView.image = anImage;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    self.title = resultString;
    
    return YES;
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSString *resultString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    self.textViewHeightConstraint.constant = [self textViewHeightForText:resultString];
    
    CGRect visibleRect = [self.textView convertRect: [textView caretRectForPosition: textView.selectedTextRange.end]
                                             toView:self.scrollView];
    visibleRect.size.height += textView.font.lineHeight;
    
    [self.scrollView scrollRectToVisible:visibleRect
                                animated:YES];
    
    return YES;
}

#pragma mark - Keyboard

- (void)keyboardWillChangeFrame:(NSNotification *)aNotification {
    CGRect keyboardFrame = [aNotification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    UIEdgeInsets insets = self.scrollView.contentInset;
    
    if (CGRectGetMaxY(keyboardFrame) >= CGRectGetHeight([UIScreen mainScreen].bounds)) {
        //keyboard is shown
        insets.bottom = CGRectGetHeight(keyboardFrame);
    } else {
        //keyboard is hidden
        insets.bottom = 0;
    }
    self.scrollView.contentInset = insets;
}

#pragma mark - WebImageControllerDelegate

- (void)webImageController:(WebImageViewController *)controller didPickImage:(UIImage *)image {
    NSData *binaryImageData = UIImagePNGRepresentation(image);
    
    NSDate *date = [NSDate date];
    
    NSString *fileName = [[NSString stringWithFormat:@"%f", [date timeIntervalSince1970]] stringByAppendingPathExtension:@"png"];
    NSString *path = [[[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                               inDomains:NSUserDomainMask] lastObject] path]
                      stringByAppendingPathComponent:fileName];
    
    self.note.imagePath = fileName;
    [self updateImageWithImage:image];
    
    [binaryImageData writeToFile:path atomically:YES];
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
