//
//  TumblrApiService.m
//  Notes
//
//  Created by Soleorax on 01.09.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import "TumblrApiService.h"

static NSString *const kBaseURL = @"https://api.tumblr.com/v2/tagged";

@implementation TumblrApiService

+ (instancetype)instance {
    static TumblrApiService *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [TumblrApiService new];
    });
    
    return instance;
}

- (void)fetchPhotosWithTag:(NSString *)aTag completion:(void (^)(NSArray<NSString *> *, NSError *))aCompletion {
    NSURLComponents *components = [[NSURLComponents alloc] initWithString:kBaseURL];
    components.queryItems = @[[NSURLQueryItem queryItemWithName:@"api_key"
                                                          value:@"hmRY9CtRaDJOtdioLrc1H2JSYc6u1XhWNQPbIHjQ06m9LUcRzI"],
                              [NSURLQueryItem queryItemWithName:@"type"
                                                          value:@"photo"],
                              [NSURLQueryItem queryItemWithName:@"tag"
                                                          value:aTag]];
    [[[NSURLSession sharedSession] dataTaskWithURL:components.URL
                                 completionHandler:^(NSData * _Nullable data,
                                                     NSURLResponse * _Nullable response,
                                                     NSError * _Nullable error)
      {
          NSDictionary *JSONdict = [NSJSONSerialization JSONObjectWithData:data
                                                                   options:NSJSONReadingAllowFragments
                                                                     error:nil];
          NSMutableArray<NSString *> *URLsArray = [NSMutableArray array];
          NSArray *responseArray = JSONdict[@"response"];
          for (NSDictionary *postDict in responseArray) {
              if ([postDict[@"type"] isEqualToString:@"photo"]) {
                  NSDictionary *firstPhoto = [postDict[@"photos"] firstObject];
                  NSDictionary *originalSizeDict = firstPhoto[@"original_size"];
                  NSString *imageURL = originalSizeDict[@"url"];
                  [URLsArray addObject:imageURL];
              }
          }
          if (nil != aCompletion) {
              aCompletion(URLsArray, nil);
          }
      }] resume];
}

@end
