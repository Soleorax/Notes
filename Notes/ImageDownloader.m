//
//  ImageDownloader.m
//  Notes
//
//  Created by Soleorax on 01.09.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import "ImageDownloader.h"

@implementation ImageDownloader

+ (instancetype)instance {
    static ImageDownloader *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [ImageDownloader new];
    });
    
    return instance;
}

- (void)downloadImageWithURL:(NSString *)anURL completion:(void (^)(UIImage *))aCompletion {
    [[[NSURLSession sharedSession] downloadTaskWithURL: [NSURL URLWithString:anURL]
                                     completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error)
      {
          if (nil != aCompletion) {
              aCompletion([UIImage imageWithData:
                           [NSData dataWithContentsOfURL:location]]);
          }
      }] resume];
}

@end
