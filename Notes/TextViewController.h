//
//  TextViewController.h
//  Notes
//
//  Created by Soleorax on 16.08.16.
//  Copyright © 2016 Soleorax. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Note.h"

@interface TextViewController : UIViewController

@property (strong, nonatomic) Note *note;
@property (assign, nonatomic) NSInteger *uniqueId;
@end
